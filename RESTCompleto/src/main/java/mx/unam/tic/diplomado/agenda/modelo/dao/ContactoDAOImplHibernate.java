package mx.unam.tic.diplomado.agenda.modelo.dao;

import mx.unam.tic.diplomado.agenda.modelo.entidades.Contacto;

public class ContactoDAOImplHibernate  extends GenericDAOImplHibernate<Contacto, Integer> 
implements ContactoDAO {

}

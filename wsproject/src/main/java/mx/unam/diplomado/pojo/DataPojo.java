package mx.unam.diplomado.pojo;


import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="data")
@XmlAccessorType(XmlAccessType.FIELD)
public class DataPojo {
	

	@XmlElement(required=true)
    private int id;
	
	@XmlElement(required=true)
	private String equipo;
	
	@XmlElement(required=true)
	private String fabricante;
	
	@XmlElement(required=true)
	private String clave;

	@XmlElement(required=true)
	private Integer existencias;


	public DataPojo() {
    }  

    public DataPojo(int id, String equipo, String fabricante) {
		super();
		this.id = id;
		this.equipo = equipo;
		this.fabricante = fabricante;
	}

	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getEquipo() {
		return equipo;
	}



	public void setEquipo(String equipo) {
		this.equipo = equipo;
	}



	public String getFabricante() {
		return fabricante;
	}



	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}



	public String getClave() {
		return clave;
	}



	public void setClave(String clave) {
		this.clave = clave;
	}



	public Integer getExistencias() {
		return existencias;
	}



	public void setExistencias(Integer existencias) {
		this.existencias = existencias;
	}

 
}
package mx.unam.diplomado;

import java.util.HashMap;
import java.util.Map;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/actions")
public class WSHelloEndPoint {
	
	@GET
	@Path("/hello")
	@Produces(MediaType.TEXT_PLAIN)
	public Response hello() {
		return Response.ok()
				.entity("Hello World from RESTFul").build();
	}
	
	@GET
	@Path("/json")
	@Produces(MediaType.APPLICATION_JSON)
	public Response helloJSon() {
		Map <String,String> m=new HashMap <String,String>();
		m.put("key", "value");
		return Response.ok()	
				.entity(m).build();
	}
	
	
	@GET
	@Path("/xml")
	@Produces(MediaType.APPLICATION_XML)
	public Response helloXML() {
		return Response.ok()
				.entity("<saludo>Hello</saludo>"					
						).build();
	}

}
